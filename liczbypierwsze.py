def czy_pierwsza(n):
    for i in range(2, n):
        if n % i == 0:
            return False
    return True

n = int(input("Podaj zakres: "))

for i in range(2,n):
    if czy_pierwsza(i):
        print(i)